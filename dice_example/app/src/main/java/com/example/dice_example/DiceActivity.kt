package com.example.dice_example

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class DiceActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dice)
        val rollButton: Button = findViewById(R.id.dice_button)
        rollButton.setOnClickListener {
            val toast=Toast.makeText(this, "ROLLED DICE !", Toast.LENGTH_SHORT).show()
            rollDice()
        }
    }

    private fun rollDice(){
        val dice=Dice(6)
        val diceRoll=dice.roll()
        val diceImage:ImageView=findViewById(R.id.dice_image_1)
        val drawableResource=when(diceRoll){
            1-> R.drawable.dice_1
            2-> R.drawable.dice_2
            3-> R.drawable.dice_3
            4-> R.drawable.dice_4
            5-> R.drawable.dice_5
            else-> R.drawable.dice_6
        }

        diceImage.setImageResource(drawableResource)
        diceImage.contentDescription=diceRoll.toString()

    }
    class Dice(val numSides : Int){
        fun roll():Int{
            return (1..numSides).random()
        }
    }
}


